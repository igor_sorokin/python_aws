#!/usr/bin/python
import boto3
import requests
import sys
import getopt
from bs4 import BeautifulSoup
from operator import itemgetter
from time import localtime, strftime


def correct_url(url):
    if not url.startswith("https://") or not url.startswith("http://"):
        try:
            url = "http://" + url
            request = requests.get(url)
        except Exception as e:
            try:
                url = "https://" + url
                request = requests.get(url)
            except Exception as e:
                print(e)
                sys.exit()
    return url


def calculate_tags(url):
    tags_dict = {}
    tags_value = 0
    request = requests.get(url)
    soup = BeautifulSoup(request.text, "html.parser")
    for tag in soup.find_all():
        tags_value += 1
        if tag.name in tags_dict:
            tags_dict[tag.name] += 1
        else:
            tags_dict[tag.name] = 1
    sorted_tags = {k: v for k, v in sorted(tags_dict.items(), key=itemgetter(0))}
    return tags_value, sorted_tags


def serialize_data(logfile, *params):
    with open(logfile, 'a+') as w_file:
        timestamp = strftime("%Y/%U/%m/%d %H:%M", localtime())
        data = ' '.join([str(string) for string in params])
        w_file.write("{} {}\n".format(timestamp, data))


def deploy_to_aws(bucket_name, logfile):
    s3 = boto3.resource("s3")
    try:
        s3.create_bucket(Bucket=bucket_name,
                         CreateBucketConfiguration={
                            "LocationConstraint": "us-west-2"
                            }
                         )
    except Exception as e:
        pass
    s3 = s3.Bucket(bucket_name)
    s3.upload_file(logfile, logfile)


def main(argv):
    bucket = ""
    logfile = ""
    url = ""
    try:
        opts, args = getopt.getopt(argv, "hu:b:f:", ["url=", "bucket=", "logfile="])
    except getopt.GetoptError:
        print("web_scrapper.py -u <url> -b <bucket_name> -f <logfile_name>")
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-u", "--url"):
            url = arg
        elif opt in ("-b", "--bucket"):
            bucket = arg
        elif opt in ("-f", "--logfile"):
            logfile = arg
    url = correct_url(url)
    value, tags = calculate_tags(url)
    serialize_data(logfile, url, value, tags)
    deploy_to_aws(bucket, logfile)
    print("Success!")

if __name__ == "__main__":
    main(sys.argv[1:])
